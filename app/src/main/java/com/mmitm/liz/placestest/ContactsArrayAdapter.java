package com.mmitm.liz.placestest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Liz on 25/08/2016.
 * Adapter to show the Places List we got from the JSON.
 */
public class ContactsArrayAdapter extends ArrayAdapter<Contact> {
    List<Contact> objects;

    public ContactsArrayAdapter(Context context, int resource, List<Contact> objects) {
        super(context, resource, objects);
        this.objects = objects;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        Contact contact = objects.get(position);
        View view = convertView;

        //If received view is null, inflate with default view.
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.contact_list_item, null);
        }
        TextView name = (TextView) view.findViewById(R.id.contactName);

        //Set the name of the place to display in list fragment
        if (name != null) {
            name.setText(contact.getName());
        }
        return view;
    }
}
