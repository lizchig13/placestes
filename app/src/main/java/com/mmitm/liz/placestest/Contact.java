package com.mmitm.liz.placestest;

/**
 * Created by Liz on 01/09/2016.
 * Represents a contact person from the contact list
 */
public class Contact {
    String mName;

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        mNumber = number;
    }

    String mNumber;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
