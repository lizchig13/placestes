package com.mmitm.liz.placestest;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

public class MapActivity extends FragmentActivity  implements OnMapReadyCallback{

    private Intent mIntent;
    private Bundle mData;
    private String mPlaceName;
    private LatLng mPlaceLocation;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.placesMap);
        mapFragment.getMapAsync(this);

        try {
            getData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.addMarker(new MarkerOptions().position(mPlaceLocation).title(mPlaceName));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(16));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(mPlaceLocation));

    }

    private void getData() throws JSONException {
        mIntent = getIntent();
        mData = mIntent.getExtras();

        JSONObject location = new JSONObject(mData.getString("location"));
        double lat = location.getDouble("latitude");
        double lng = location.getDouble("longitude");
        mPlaceLocation = new LatLng(lat,lng);
        mPlaceName = mData.getString("name");


    }
}
