package com.mmitm.liz.placestest;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Liz on 10/09/2016.
 */
public class SendInvitation extends AsyncTask<String, Integer, Void> {
    @Override
    protected Void doInBackground(String... params) {
        URL requestUrl = null;
        HttpURLConnection urlConnection;
        OutputStream printOut;
        String request = params[0];

        try {
            requestUrl = new URL("https://gcm-http.googleapis.com/gcm/send");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            urlConnection = (HttpURLConnection) requestUrl.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type","application/json");
            urlConnection.setRequestProperty("Authorization","key=AIzaSyC0KNctyUr12AOKLw9teYx3qfuRmgLNJ7c");
            urlConnection.setDoOutput(true);
            printOut = new BufferedOutputStream(urlConnection.getOutputStream());
            printOut.write(request.getBytes());
            printOut.flush ();
            printOut.close ();

            String response = urlConnection.getResponseMessage();
            Log.d("Response",response);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
