package com.mmitm.liz.placestest;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by Liz on 08/09/2016.
 */
public class TokenRefreshListenerService extends InstanceIDListenerService{
    @Override
    public void onTokenRefresh(){
        Intent intent = new Intent(this,RegistrationService.class);
        startService(intent);
    }
}
