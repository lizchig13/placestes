package com.mmitm.liz.placestest;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by Liz on 08/09/2016.
 */
public class NotificationsListenerService extends GcmListenerService {

    @Override
    public void onMessageReceived(String from,Bundle data){
        String notification = data.getString("notification");
        Intent intent = new Intent(this,ReceivingMessageActivity.class);
        intent.putExtra("notification",notification);

    }
}
