package com.mmitm.liz.placestest;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ContactsActivity extends AppCompatActivity implements ContactsListFragment.OnHeadlineSelectedListener {

    List<Contact> mContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        mContacts = new ArrayList<>();
        try {
            readContactsJson();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ((GlobalsClass)this.getApplication()).setContactList(mContacts);
        Intent intent = new Intent(this,RegistrationService.class);
        this.startService(intent);
    }

    public void readContactsJson() throws IOException {
        Resources res = this.getResources();
        InputStream locations = res.openRawResource(R.raw.contacts);
        JsonReader jsonReader = new JsonReader(new InputStreamReader(locations));

        jsonReader.beginObject();
        while(jsonReader.hasNext()){
            String name = jsonReader.nextName();
            if(name.equals("Contact")){
                ReadContactsArray(jsonReader);
            }
            else{
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
    }
    private void ReadContactsArray(JsonReader jsonReader) throws IOException {

        jsonReader.beginArray();
        while(jsonReader.hasNext()){
            Contact contact = ReadJsonContactObject(jsonReader);
            mContacts.add(contact);
        }
        jsonReader.endArray();
    }
    private Contact ReadJsonContactObject(JsonReader jsonReader) throws IOException {
        Contact contact;

        contact = new Contact();
        jsonReader.beginObject();
        while(jsonReader.hasNext()){
            String name = jsonReader.nextName();
            switch(name){
                case "name":
                    contact.setName(jsonReader.nextString());
                    break;
                case "number":
                    contact.setNumber(jsonReader.nextString());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        return contact;
    }

    @Override
    public void onArticleSelected(int position) {
        Intent intent = new Intent(this,SendingRequestActivity.class);
        startActivity(intent);
    }
}
