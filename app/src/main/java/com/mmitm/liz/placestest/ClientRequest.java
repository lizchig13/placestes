package com.mmitm.liz.placestest;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Socket;

/**
 * Created by Liz on 05/09/2016.
 */
public class ClientRequest extends AsyncTask {

    String mDestAddress;
    int mDestPort;

    ClientRequest(String invitedName, String invitedNumber, LatLng requestingLocation, String type, String message) throws JSONException {
        JSONObject request = BuildJsonRequest(invitedName,invitedNumber,requestingLocation,type,message);

    }
    @Override
    protected Object doInBackground(Object[] params) {
        Socket socket = null;



        return null;
    }
    private JSONObject BuildJsonRequest(String invitedName, String invitedNumber, LatLng requestingLocation, String type, String message) throws JSONException {
        JSONObject invitedUser = new JSONObject().put("name",invitedName)
                .put("phone", invitedNumber);
        JSONObject userLocation = new JSONObject().put("lng",requestingLocation.longitude)
                .put("lat",requestingLocation.latitude);
        JSONObject requestingUser = new JSONObject().put("name", "Liz")
                .put("phone", "0508134889")
                .put("location",userLocation);
        JSONObject request = new JSONObject().put("invited_user",invitedUser)
                .put("requesting_user",requestingUser)
                .put("place_type",type)
                .put("message",message);

        return  request;
    }
}
