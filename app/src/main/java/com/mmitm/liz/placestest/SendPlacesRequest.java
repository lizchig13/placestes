package com.mmitm.liz.placestest;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Liz on 09/09/2016.
 */
public class SendPlacesRequest extends AsyncTask<URL,Void,String> {
    private Context mContext;

    public SendPlacesRequest(Context context){
        mContext = context;
    }

    protected String doInBackground(URL... params) {
        URL url = params[0];
        HttpURLConnection urlConnection = null;
        InputStreamReader in = null;
        StringBuilder total = new StringBuilder();
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            in = new InputStreamReader(urlConnection.getInputStream());
            BufferedReader r = new BufferedReader(in);
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(urlConnection != null)
                urlConnection.disconnect();
        }
        return total.toString();
    }

    public void onPostExecute(String result){
        List<Place> places;

        ReadJsonPlacesClass readJsonPlacesClass = new ReadJsonPlacesClass();
        places = null;
        //Parse the JSON that is in raw resources.
        try {
            places = readJsonPlacesClass.ReadJson(result);
        }
        catch (IOException e) {
        }
        if(places != null)
            ((GlobalsClass)mContext.getApplicationContext()).setPlaceList(places);

    }
}
