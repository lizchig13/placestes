package com.mmitm.liz.placestest;

        import android.content.Context;
        import android.content.res.Resources;
        import android.util.JsonReader;

        import com.google.android.gms.maps.model.LatLng;

        import org.json.JSONObject;

        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.io.StringReader;
        import java.util.ArrayList;
        import java.util.List;

public class ReadJsonPlacesClass {

    List<Place> mPlaces;

    public ReadJsonPlacesClass(){
        mPlaces = new ArrayList<>();
    }

    public List<Place> ReadJson(String in) throws IOException {

      //  JsonReader jsonReader = new JsonReader(new StringReader(placesJson));
        JsonReader jsonReader = new JsonReader(new StringReader(in));

        jsonReader.beginObject();
        while(jsonReader.hasNext()){
            String name = jsonReader.nextName();
            if(name.equals("results")){
                ReadResultsArray(jsonReader);
            }
            else{
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        return mPlaces;
    }

    private void ReadResultsArray(JsonReader jsonReader) throws IOException {

        jsonReader.beginArray();
        while(jsonReader.hasNext()){
            Place place = ReadJsonResultObject(jsonReader);
            mPlaces.add(place);
        }
        jsonReader.endArray();
    }

    private Place ReadJsonResultObject(JsonReader jsonReader) throws IOException {
        Place place;

        place = new Place();
        jsonReader.beginObject();
        while(jsonReader.hasNext()){
            String name = jsonReader.nextName();
            switch(name){
                case "geometry":
                    place.setLocation(readGeometryObject(jsonReader));
                    break;
                case "icon":
                    place.setIconURL(jsonReader.nextString());
                    break;
                case "name":
                    place.setName(jsonReader.nextString());
                    break;
                case "rating":
                    place.setRating(jsonReader.nextDouble());
                    break;
                case "vicinity":
                    place.setAddress(jsonReader.nextString());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        return place;
    }

    private LatLng readGeometryObject(JsonReader jsonReader) throws IOException {

        LatLng latlng = new LatLng(-1,-1);

        jsonReader.beginObject();
        while (jsonReader.hasNext()){
            String name = jsonReader.nextName();
            if(name.equals("location")){
                latlng = readLocationObject(jsonReader);
            }
            else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        return latlng;
    }

    private LatLng readLocationObject(JsonReader jsonReader) throws IOException {

        double lat = -1;
        double lng = -1;

        jsonReader.beginObject();
        while(jsonReader.hasNext()){
            String name = jsonReader.nextName();
            if(name.equals("lat"))
                lat = jsonReader.nextDouble();
            else if (name.equals("lng"))
                lng = jsonReader.nextDouble();
            else
                jsonReader.skipValue();
        }
        jsonReader.endObject();
        return new LatLng(lat,lng);
    }

}
