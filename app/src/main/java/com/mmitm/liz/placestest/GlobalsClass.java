package com.mmitm.liz.placestest;

import android.app.Application;

import java.util.List;

/**
 * Created by Liz on 25/08/2016.
 * This global class holds all the global variables. Can be accessed from anywhere in the code.
 */
public  class GlobalsClass extends Application {

    public List<Place> getPlaceList() {
        return mPlaceList;
    }

    public void setPlaceList(List<Place> placeList) {
        mPlaceList = placeList;
    }

    public List<Place> mPlaceList;

    public List<Contact> getContactList() {
        return mContactList;
    }

    public void setContactList(List<Contact> contactList) {
        mContactList = contactList;
    }

    public List<Contact> mContactList;
}
