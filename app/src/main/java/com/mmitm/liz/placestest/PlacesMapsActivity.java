package com.mmitm.liz.placestest;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class PlacesMapsActivity extends FragmentActivity implements OnMapReadyCallback, PlacesList.OnHeadlineSelectedListener{

    private GoogleMap mMap;
    private List<Place> mPlaceList;
    private LatLng mMidPoint;
    private Place mCurrPlace;
    private Button mSendButton;
    private Snackbar mSnackbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Bundle bundle = getIntent().getParcelableExtra("bundle");
        mMidPoint = bundle.getParcelable("midPoint");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mPlaceList = ((GlobalsClass)getApplicationContext()).getPlaceList();

        mSendButton = (Button) findViewById(R.id.sendLocationButton);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    sentPlaceNotification();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.addMarker(new MarkerOptions().position(mMidPoint).title("Middle Point"));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(16));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(mMidPoint));
    }

    @Override
    public void onArticleSelected(int position) {
        mMap.clear();
        mCurrPlace = mPlaceList.get(position);
        mMap.addMarker(new MarkerOptions().position(mCurrPlace.getLocation()).title(mCurrPlace.getName()));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(16));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(mCurrPlace.getLocation()));
    }
    private void sentPlaceNotification() throws JSONException {
        JSONObject location = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject notification = new JSONObject();
        JSONObject request = new JSONObject();
        LatLng placeLocation = mCurrPlace.getLocation();

        location.put("latitude",Double.toString(placeLocation.latitude));
        location.put("longitude",Double.toString(placeLocation.longitude));

        data.put("name",mCurrPlace.getName());
        data.put("location",location);

        notification.put("click_action","OPEN_MAP_Activity");
        notification.put("body","Liz has chosen a location");
        notification.put("title","Meet Me In The Middle");
        notification.put("icon","ic_location");


        request.put("to","/topics/meeting_accepted_topic");
        request.put("data",data);
        request.put("notification", notification);

        String stringRequest = request.toString();
        stringRequest = stringRequest.replace("\\","");
        Log.d("JsonRequest",request.toString());

        //Send request to gcm
        new SendInvitation().execute(stringRequest);
        updateUser();
    }
    private void updateUser(){
        mSnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout),R.string.messageSent,Snackbar.LENGTH_LONG);
        mSnackbar.show();
    }
}
