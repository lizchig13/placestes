package com.mmitm.liz.placestest;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Liz on 19/08/2016.
 */
public class Place implements Parcelable{

    protected Place(Parcel in) {
        mLocation = in.readParcelable(LatLng.class.getClassLoader());
        mName = in.readString();
        mIconURL = in.readString();
        mRating = in.readDouble();
        mAddress = in.readString();
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };

    public Place() {

    }

    public LatLng getLocation() {
        return mLocation;
    }

    public void setLocation(LatLng location) {
        mLocation = location;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getIconURL() {
        return mIconURL;
    }

    public void setIconURL(String iconURL) {
        mIconURL = iconURL;
    }

    public double getRating() {
        return mRating;
    }

    public void setRating(double rating) {
        mRating = rating;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    private LatLng mLocation;
    private String mName;
    private String mIconURL;
    private double mRating;
    private String mAddress;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mLocation, flags);
        dest.writeString(mName);
        dest.writeString(mIconURL);
        dest.writeDouble(mRating);
        dest.writeString(mAddress);
    }
}
