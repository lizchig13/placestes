package com.mmitm.liz.placestest;

import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SendingRequestActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int MY_PERMISSION_ACCESS_COURSE_LOCATION = 1000;
    private Button mSendButton;
    private Snackbar mSnackbar;
    private String mSelectedPlace;
    private EditText mEditText;
    private GoogleApiClient mGoogleApiClient;
    private Location mReadLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sending_request);

        mEditText = (EditText) findViewById(R.id.composeMessage);
        Spinner spinner = (Spinner) findViewById(R.id.placesSpinner);
        spinner.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.places_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        mReadLocation = new Location("ReadLocation");
        mReadLocation.setLatitude(32.047945);
        mReadLocation.setLongitude(34.761118);

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mSendButton = (Button) findViewById(R.id.sendButton);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    sendMeetingRequest();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
        mSnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), R.string.messageSent, Snackbar.LENGTH_LONG);
        mSnackbar.setAction(R.string.close, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                    MY_PERMISSION_ACCESS_COURSE_LOCATION );
        }
        mReadLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mReadLocation != null) {
            double latitude = mReadLocation.getLatitude();
            Log.d("Location read - lat", String.valueOf(latitude));
            double longitude = mReadLocation.getLongitude();
            Log.d("Location read - long", String.valueOf(longitude));
        }
        else{
            mReadLocation = new Location("ReadLocation");
            mReadLocation.setLatitude(32.047945);
            mReadLocation.setLongitude(34.761118);
        }
    }
    public void onConnectionSuspended(int i) {

    }

    public void sendMeetingRequest() throws JSONException, MalformedURLException {

        sendRequestToCloud();
        //Close the message activity.
        mSnackbar.show();
    }

    //When one of the places objects is selected.
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Resources res = getResources();
        String[] places = res.getStringArray(R.array.places_array);
        mSelectedPlace = (places[position]).toLowerCase();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private String buildJsonRequest() throws JSONException {
        JSONObject location = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject notification = new JSONObject();
        JSONObject request = new JSONObject();

        location.put("latitude",String.valueOf(mReadLocation.getLatitude()));
        location.put("longitude",String.valueOf(mReadLocation.getLongitude()));

        data.put("name","Liz");
        data.put("phone","0508134889");
        data.put("place_type",mSelectedPlace);
        data.put("message",mEditText.getText().toString());
        data.put("location",location);

        notification.put("click_action","OPEN_ACTIVITY_1");
        notification.put("body","New meeting request from Liz");
        notification.put("title","Meet Me In The Middle");
        notification.put("icon","ic_location");


        request.put("to","/topics/meeting_request_topic");
        request.put("data",data);
        request.put("notification", notification);

        String stringRequest = request.toString();
        stringRequest = stringRequest.replace("\\","");
        Log.d("JsonRequest",request.toString());
        return stringRequest;
    }

    private void sendRequestToCloud() throws JSONException, MalformedURLException {
        String request = buildJsonRequest();
        new SendInvitation().execute(request);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mReadLocation.setLatitude(32.047945);
        mReadLocation.setLongitude(34.761118);
    }
}
