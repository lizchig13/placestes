package com.mmitm.liz.placestest;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.ExecutionException;

public class ReceivingMessageActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private static final int MY_PERMISSION_ACCESS_COURSE_LOCATION = 1000;
    private Button mAcceptButton;
    private Button mDeclineButton;
    private TextView mMessage;
    Intent mIntent;
    private Bundle mData;
    private LatLng mReceivedLocation;
    private LatLng mMidPoint;
    private URL mURL;
    private GoogleApiClient mGoogleApiClient;
    private Location mReadLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiving_message);

        mAcceptButton = (Button) findViewById(R.id.acceptButton);
        mDeclineButton = (Button) findViewById(R.id.declineButton);
        mMessage = (TextView) findViewById(R.id.messageText);

        mReadLocation = new Location("ReadLocation");
        mReadLocation.setLatitude(32.047945);
        mReadLocation.setLongitude(34.761118);

        getData();
        mAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AcceptMessage();
            }
        });
        mAcceptButton.setClickable(false);
        mAcceptButton.setTextColor(getResources().getColor(R.color.Disabled));
        mDeclineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Toast toast = Toast.makeText(ReceivingMessageActivity.this,"Sending decline", Toast.LENGTH_LONG);
                toast.show();
            }
        });
        try {
            displayMessage();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //LatLng currLocation = new LatLng(32.047727,34.760964);
        try {
            JSONObject location = new JSONObject(mData.getString("location"));
            double lat = location.getDouble("latitude");
            double lng = location.getDouble("longitude");
            mReceivedLocation = new LatLng(lat,lng);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        //mMidPoint = calculateMidPoint(new LatLng(mReadLocation.getLatitude(),mReadLocation.getLongitude()),mReceivedLocation);

    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void AcceptMessage(){
        Intent placesIntent = new Intent(this,PlacesMapsActivity.class);
        Bundle midPoint = new Bundle();
        midPoint.putParcelable("midPoint",mMidPoint);
        placesIntent.putExtra("bundle",midPoint);
        startActivity(placesIntent);
    }
    private void getData(){
        mIntent = getIntent();
        mData = mIntent.getExtras();
    }
    private void displayMessage() throws IOException {
        String message = mData.getString("message");
        mMessage.setText(message);
    }
    private LatLng calculateMidPoint(LatLng firstLocation, LatLng secondLocation){

        double lon1,lon2;
        double lat1, lat2;

        lon1 = firstLocation.longitude;
        lon2 = secondLocation.longitude;
        lat1 = firstLocation.latitude;
        lat2 = secondLocation.latitude;

        double dLon = Math.toRadians(lon2 - lon1);
        //convert to radians
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        lon1 = Math.toRadians(lon1);

        double Bx = Math.cos(lat2) * Math.cos(dLon);
        double By = Math.cos(lat2) * Math.sin(dLon);
        double lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
        double lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);
        lat3 = Math.toDegrees(lat3);
        lon3 = Math.toDegrees(lon3);

        return new LatLng(lat3,lon3);
    }

    private void setRequestURL() throws IOException, ExecutionException, InterruptedException {

        double latitude = mMidPoint.latitude;
        double longitude = mMidPoint.longitude;
        String type = URLEncoder.encode(mData.getString("place_type"),"UTF-8");

        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(url)
                .append("key=" + "AIzaSyD7gHVstnt9Cnv5AdadELVd_cuj3Hg6-1Q")
                .append("&location=")
                .append(Double.toString(latitude) + "," + Double.toString(longitude))
                .append("&radius=1000")
                .append("&type=" + type);
        mURL = new URL(stringBuilder.toString());
        Log.d("URLRequest", url);
        new SendPlacesRequest(this).execute(mURL);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSION_ACCESS_COURSE_LOCATION);
        }
        mReadLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mReadLocation != null) {
            double latitude = mReadLocation.getLatitude();
            Log.d("Location read - lat", String.valueOf(latitude));
            double longitude = mReadLocation.getLongitude();
            Log.d("Location read - long", String.valueOf(longitude));
        } else {
            mReadLocation = new Location("ReadLocation");
            mReadLocation.setLatitude(32.047945);
            mReadLocation.setLongitude(34.761118);
        }
        mMidPoint = calculateMidPoint(new LatLng(mReadLocation.getLatitude(), mReadLocation.getLongitude()), mReceivedLocation);
        try {
            setRequestURL();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mAcceptButton.setClickable(true);
        mAcceptButton.setTextColor(getResources().getColor(R.color.colorAccent));
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mMidPoint = calculateMidPoint(new LatLng(mReadLocation.getLatitude(),mReadLocation.getLongitude()),mReceivedLocation);
        try {
            setRequestURL();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mAcceptButton.setClickable(true);
        mAcceptButton.setTextColor(getResources().getColor(R.color.colorAccent));
    }
}
